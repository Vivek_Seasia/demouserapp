﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DemoUserApp.ViewModel
{
    public class UserInfoView
    {
        public string UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        public string Address { get; set; }
        [Required]
        [StringLength(15)]
        public string ContactNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsActive { get; set; }
    }
}
