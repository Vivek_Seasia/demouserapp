﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DemoUserApp.Models;
using DemoUserApp.Services;
using DemoUserApp.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoUserApp.Controllers
{
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserService _user;
        public UserController(IUserService user)
        {
            _user = user;
        }

        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> Create([FromBody]UserInfoView userInfo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _user.Create(userInfo);
                    if (response)
                    {
                        return Json(new { statusCode = HttpStatusCode.OK, Message = "User created" });
                    }
                    else
                    {
                        return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Some error occured while creating users" });
                    }
                }
                return  Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Incorrect input" });
            }
            catch(Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = ex.Message });
            }
        }

        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var users = await _user.GetAll();
                if (users.Any())
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "Users found", Data = new { users } });
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "No users found", Data = new { } });
                }
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = ex.Message, Data = new { } });
            }
        }
        [Route("getbyid")]
        public async Task<IActionResult> GetById(string userId)
        {
            try
            {
                var user = await _user.GetUserById(userId);
                if (user != null)
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "User found", Data = new { user } });
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "No user found", Data = new { } });
                }
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = ex.Message, Data = new { } });
            }
        }
        [HttpPost]
        [Route("update")]
        public async Task<ActionResult> Update([FromBody]UserInfoView userInfo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = await _user.Update(userInfo);
                    if (response)
                    {
                        return Json(new { statusCode = HttpStatusCode.OK, Message = "User updated" });
                    }
                    else
                    {
                        return Json(new { statusCode = HttpStatusCode.OK, Message = "Unable to update. User not found." });
                    }
                }
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = "Incorrect input" });
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = ex.Message });
            }
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult> Delete(string UserId)
        {
            try
            {
                var response = await _user.Delete(UserId);
                if (response)
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "User deleted" });
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "Unable to delete. User not found."});
                }
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = ex.Message });
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        public async Task<ActionResult> UpdateStatus([FromBody]UserInfoView userInfo)
        {
            try
            {
                var response = await _user.UpdateStatus(userInfo.UserId, userInfo.IsActive);
                if (response)
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "User status updated" });
                }
                else
                {
                    return Json(new { statusCode = HttpStatusCode.OK, Message = "Unable to update status. User not found." });
                }
               
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = HttpStatusCode.BadRequest, Message = ex.Message });
            }
        }
    }
}