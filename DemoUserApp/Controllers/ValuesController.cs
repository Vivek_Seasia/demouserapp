﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoUserApp.Data;
using DemoUserApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace DemoUserApp.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        MongoDbContext _db;
        public ValuesController(MongoDbContext db)
        {
            _db = db;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async void Post([FromBody]string value)
        {
            try
            {


                UserInfo user = new UserInfo();
                user.UserId = Guid.NewGuid().ToString();
                user.UserName = "VIVEK";

                await _db.UserInfo.InsertOneAsync(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
