﻿using DemoUserApp.Models;
using DemoUserApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoUserApp.Services
{
    public interface IUserService
    {
        Task<bool> Create(UserInfoView user);
        Task<ICollection<UserInfo>> GetAll();
        Task<UserInfo> GetUserById(string id);
        Task<bool> Update(UserInfoView user);
        Task<bool> UpdateStatus(string id, bool status);
        Task<bool> Delete(string id);
    }
}
