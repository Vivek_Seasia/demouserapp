﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoUserApp.Data;
using DemoUserApp.Models;
using DemoUserApp.ViewModel;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DemoUserApp.Services
{
    public class UserService : IUserService
    {
        MongoDbContext _db;
        public UserService(MongoDbContext db)
        {
            _db = db;
        }
        public async Task<bool> Create(UserInfoView user)
        {
            try
            {
                var users = new UserInfo()
                {
                    UserId = Guid.NewGuid().ToString(),
                    Address = user.Address,
                    ContactNo = user.ContactNo,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    UserName = user.UserName,
                    IsActive = true
                };
                await _db.UserInfo.InsertOneAsync(users);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Delete(string id)
        {
            try
            {
                var userObject = _db.UserInfo.Find(x => x.UserId == id).FirstOrDefault();
                if (userObject != null)
                {
                    await _db.UserInfo.DeleteOneAsync(x => x.UserId == id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ICollection<UserInfo>> GetAll()
        {
            try
            {
                return await _db.UserInfo.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UserInfo> GetUserById(string id)
        {
            try
            {
                return await _db.UserInfo.Find(x => x.UserId == id).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateStatus(string id, bool status)
        {
            try
            {
                var userObject = _db.UserInfo.Find(x => x.UserId == id).FirstOrDefault();
                if (userObject != null)
                {
                    var updates = new List<UpdateDefinition<UserInfo>>();
                    updates.Add(Builders<UserInfo>.Update.Set(x => x.IsActive, status));
                    await _db.UserInfo.FindOneAndUpdateAsync(Builders<UserInfo>.Filter.Eq(x => x.UserId, id),
                        Builders<UserInfo>.Update.Combine(updates),
                        new FindOneAndUpdateOptions<UserInfo, UserInfo> { ReturnDocument = ReturnDocument.After });

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(UserInfoView user)
        {
            try
            {
                var userObject = _db.UserInfo.Find(x => x.UserId == user.UserId).FirstOrDefault();
                if (userObject != null)
                {
                    var updates = new List<UpdateDefinition<UserInfo>>();

                    updates.Add(Builders<UserInfo>.Update.Set(x => x.ModifiedDate, DateTime.Now));
                    updates.Add(Builders<UserInfo>.Update.Set(x => x.UserName, user.UserName));
                    updates.Add(Builders<UserInfo>.Update.Set(x => x.Address, user.Address));
                    updates.Add(Builders<UserInfo>.Update.Set(x => x.ContactNo, user.ContactNo));
                    await _db.UserInfo.FindOneAndUpdateAsync(Builders<UserInfo>.Filter.Eq(x => x.UserId, user.UserId),
                        Builders<UserInfo>.Update.Combine(updates),
                        new FindOneAndUpdateOptions<UserInfo, UserInfo> { ReturnDocument = ReturnDocument.After });

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
