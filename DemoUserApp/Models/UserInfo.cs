﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace DemoUserApp.Models
{
    public class UserInfo
    {
        [BsonId]
        [BsonElement("id")]
        public string UserId { get; set; }

        [BsonElement("username")]
        public string UserName { get; set; }

        [BsonElement("address")]
        public string Address { get; set; }


        [BsonElement("contactno")]
        public string ContactNo { get; set; }


        [BsonElement("createddate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("modifieddate")]
        public DateTime ModifiedDate { get; set; }
        [BsonElement("isactive")]
        public bool IsActive { get; set; }

    }
}
